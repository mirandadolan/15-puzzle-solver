# Miranda Dolan's source code for assignment 1
# Septemeber 20, 2018
# solves 15 puzzle using BFS and A* with two different heuristics

import copy
import sys
from collections import deque


class GameState():
    ##################################################################
    # GAMESTATE CLASS CREATED OBECTS
    # - all necessary attributetes are given here
    # - cost is assigned per node then used to calculate g(n) later
    ##################################################################
    def __init__(self, state_id, parent_id, parent):
        self.state_id = state_id
        self.parent_id = parent_id
        self.board = [[], [], [], []]
        self.parent = parent
        self.g_of_n = 0
        self.h_of_n = 0
        self.f_of_n = 0
        self.cost = 0
        self.priority = 0

    ### Getter/Setter Functions ###
    def get_state_id(self):
        return self.state_id

    def set_state_id(self, state_id):
        self.state_id = state_id

    def get_parent_id(self):
        return self.parent_id

    def set_parent_id(self, parent_id):
        self.parent_id = parent_id

    def get_board(self):
        return self.board

    def set_board(self, board):
        self.board = board

    def get_parent(self):
        return self.parent

    def set_parent(self, parent):
        self.parent = parent

    def get_g_of_n(self):
        return g_of_n

    def set_g_of_n(self, g_of_n):
        self.g_of_n = g_of_n

    def get_h_of_n(self):
        return h_of_n

    def set_h_of_n(self, h_of_n):
        self.h_of_n = h_of_n

    def get_f_of_n(self):
        return f_of_n

    def set_f_of_n(self, f_of_n):
        self.f_of_n = f_of_n

    def get_cost(self):
        return self.cost

    def set_cost(self, cost):
        self.cost = cost


    def print_as_matrix(self):
        ### print matrix to look like game board ###
        output = " -------------------\n"
        index = 0
        for x in self.board:
            for y in x:
                if(y < 10 and y != -1):
                    output += " {0}  |".format(str(y))
                elif x != -1:
                    output += " {0} |".format(str(y))
                else:
                    output += " |"
                if (index + 1) % 4 ==0:
                    if (index != len(x)-1):
                        output += "\n -------------------\n"
                    else:
                        output += "\n -------------------\n"
                index += 1
        print(output)
        return



def move(Board, slide, child):
    ##################################################################
    # Moves a tiles on the current board to update the child's board
    # - takes in current board and desired move
    # - returns updated board for child
    # - child.cost is updated here for calculating h1 later
    ##################################################################
    board = copy.deepcopy(Board)
    row = 0
    while 0 not in board[row]: row += 1
    col = board[row].index(0)
    if slide == 'up':
        if board[row-1][col] < 10:
          child.cost = 1
        else:
          child.cost = 10
        board[row][col]=board[row-1][col]
        board[row-1][col]=0
        # print(board)
        return board

    if slide == 'down':
        if board[row+1][col] < 10:
          child.cost = 1
        else:
          child.cost = 10
        board[row][col]=board[row+1][col]
        board[row+1][col]=0
        # print(board)
        return board

    if slide=='left':
        if board[row][col-1] < 10:
          child.cost = 1
        else:
          child.cost = 10
        board[row][col]=board[row][col-1]
        board[row][col-1]=0
        # print(board)
        return board

    if slide=='right':
        if board[row][col+1] < 10:
          child.cost = 1
        else:
          child.cost = 10
        board[row][col]=board[row][col+1]
        board[row][col+1]=0
        # print(board)
        return board

def flatten(board):
    ### Creates a 1D vector from a given matrix (used for h2) ###
    vec_board = []
    for row in board:
        for col in row:
            vec_board.append(col)
    return vec_board


def manhattan(board):
    # The estimated cost of taking a board B to the goal state G is the sum of
    # the smallest number of moves for each tile, that is not at its final location,
    # to reach its final location as required by G.
    vec_board = flatten(board)
    return sum(abs((val - 1) % 4 - i % 4) + abs((val - 1) // 4 - i // 4) for i, val in enumerate(vec_board) if val)


def estCost(board, goal_board):
    # The estimated cost of taking a board B to the goal state G is the number of
    # tiles in B that are not in the correct location as required by G.
    cost = 0
    myboard = board
    compare = goal_board
    for i in range(4):
        for j  in range(4):
            if myboard[i][j] != compare[i][j]:
                if myboard[i][j] < 10:
                    cost += 1

                else:
                    cost += 10
    return cost


def getResultsBFS(game_state, open_count):
    ##################################################################
    # Prints state information for states in final path
    # - creates list by referrencing parent of each node from goal back to start
    # - reverses the list then prints info start to goal
    ##################################################################
    g_val = 0
    pathList=[]
    while game_state != None:
        pathList.append(game_state)
        game_state = game_state.parent
    pathList.reverse()
    for i in range(0, len(pathList)):
        g_val += pathList[i].cost
        pathList[i].g_of_n = g_val
        pathList[i].f_of_n = pathList[i].g_of_n + pathList[i].h_of_n
        print("***************************************")
        print("Node Cost: ", pathList[i].cost)
        print("State ID: ", pathList[i].state_id)
        print("Parent ID: ", pathList[i].parent_id)
        print("Priority: ", pathList[i].priority)
        print("g(n): ",pathList[i].g_of_n)
        print("h(n): ",pathList[i].h_of_n)
        print("f(n): ", pathList[i].f_of_n)
        pathList[i].print_as_matrix()

    print("Total Nodes Opened: ", open_count )
    print("Total Nodes Closed: ", len(closed))
    print("Moves from Start to Goal: ", len(pathList))
    return 0

def getResultsAstar(game_state, open_count, closed_count):
    ##################################################################
    # Prints state information for states in final path
    # - creates list by referrencing parent of each node from goal back to start
    # - reverses the list then prints info start to goal
    ##################################################################
    g_val = 0
    pathList=[]
    while game_state != None:
        pathList.append(game_state)
        game_state = game_state.parent
    pathList.reverse()
    for i in range(0, len(pathList)):
        print("***************************************")
        print("Node Cost: ", pathList[i].cost)
        print("State ID: ", pathList[i].state_id)
        print("Parent ID: ", pathList[i].parent_id)
        print("Priority: ", pathList[i].priority)
        print("g(n): ",pathList[i].g_of_n)
        print("h(n): ",pathList[i].h_of_n)
        print("f(n): ", pathList[i].f_of_n)
        pathList[i].print_as_matrix()

    print("Total Nodes Opened: ", open_count )
    print("Total Nodes Closed: ", closed_count)
    print("Moves from Start to Goal: ", len(pathList))
    return 0


def get_g_f_val(game_state):
    ### Calculates g, f, and priority values when using A* ###
    g_val = 0
    pathList=[]
    while game_state != None:
        pathList.append(game_state)
        game_state = game_state.parent
    pathList.reverse()
    for i in range(0, len(pathList)):
        g_val += pathList[i].cost
        pathList[i].g_of_n = g_val
        pathList[i].f_of_n = pathList[i].g_of_n + pathList[i].h_of_n
        pathList[i].priority = pathList[i].f_of_n



def astar(h_choice):
    ##################################################################
    # A* SEARCH ALGORITHM
    # - adds root as start of open list
    # - expands children based on priority parent has in open list
    # - doesn't add child to open if same board already exists in open with higher priority
    # - calls "getResultsAstar" to print the correct path from start to goal
    ##################################################################
    print("\nExecuting A* Search using heuristic", h_choice, ". . .\n")
    state_id = 0
    h = h_choice
    open_count = 1
    closed_count = 0
    comp_f = []
    while len(open) > 0:
        children = []
        for o in open: # LOOPS FINDS INDEX OF LOWEST F VALUE IN OPEN
            comp_f.append(o.f_of_n)
        q_ind = comp_f.index(min(comp_f))
        q = open[q_ind]
        curr_board = copy.deepcopy(q.board)
        del open[q_ind] # POP LOWEST F OFF OPEN QUEUE
        closed.append(q)
        closed_count += 1
        if q.board == goal:
            getResultsAstar(q, open_count)
            print("\A* Search is finished!\n")
            break;

        row = 0
        while 0 not in curr_board[row]:
            row += 1
        col = curr_board[row].index(0);


        if col > 0: # MOVE LEFT
            state_id += 1
            child_state = GameState(state_id, q.state_id, copy.deepcopy(q))
            child_state.board = move(curr_board,'left', child_state)
            if h == 1:
                child_state.h_of_n = estCost(child_state.board, goal)
            else:
                child_state.h_of_n = manhattan(child_state.board)
            get_g_f_val(child_state)
            if child_state.board == goal:
                getResultsAstar(child_state, open_count, closed_count)
                print("\A* Search is finished!\n")
                break;
            children.append(child_state)

        if col < 3: # MOVE RIGHT
            state_id += 1
            child_state = GameState(state_id, q.state_id, copy.deepcopy(q))
            child_state.board = move(curr_board,'right', child_state)
            if h == 1:
                child_state.h_of_n = estCost(child_state.board, goal)
            else:
                child_state.h_of_n = manhattan(child_state.board)
            get_g_f_val(child_state)
            if child_state.board == goal:
                getResultsAstar(child_state, open_count, closed_count)
                print("\nA* Search is finished!\n")
                break;
            children.append(child_state)

        if row < 3: # MOVE UP
            state_id += 1
            child_state = GameState(state_id,q.state_id, copy.deepcopy(q))
            child_state.board = move(curr_board,'down', child_state)
            if h == 1:
                child_state.h_of_n = estCost(child_state.board, goal)
            else:
                child_state.h_of_n = manhattan(child_state.board)
            get_g_f_val(child_state)
            if child_state.board == goal:
                getResultsAstar(child_state, open_count, closed_count)
                print("\nA* Search is finished!\n")
                break;
            children.append(child_state)

        if row > 0: # MOVE DOWN
            state_id += 1
            child_state = GameState(state_id,q.state_id,copy.deepcopy(q))
            child_state.board = move(curr_board,'up', child_state)
            if h == 1:
                child_state.h_of_n = estCost(child_state.board, goal)
            else:
                child_state.h_of_n = manhattan(child_state.board)
            get_g_f_val(child_state)
            if child_state.board == goal:
                getResultsAstar(child_state, open_count, closed_count)
                print("\nA* Search is finished!\n")
                break;
            children.append(child_state)

        closed_boards = []
        open_boards = []
        for c in closed:
            closed_boards.append(c.board)

        for o in open:
            open_boards.append(o.board)

        for child in children:
            if child.board in closed_boards:
                continue
            elif child.board in open_boards:
                i = open_boards.index(child.board)
                if child.f_of_n > open[i].f_of_n:
                    continue
            open.append(child)
            open_count += 1



def bfs():
    #################################
    # BREADTH FIRST SEARCH ALGORITHM
    # - Expands children in order of created nodes
    # - Breaks once child matches the goal state
    # - calls "getResultsBFS" to print the correct path from start to goal
    #################################
    print("\nExecuting Breadth First Search . . .\n")
    state_id = 0
    open_count = 0
    new_root = root

    while len(open) > 0:
        new_root = open[0]
        curr_board = copy.deepcopy(new_root.board)

        row = 0
        while 0 not in curr_board[row]:
            row += 1
        col = curr_board[row].index(0);


        if col > 0: # MOVE LEFT
            state_id += 1
            child_state = GameState(state_id, new_root.state_id, copy.deepcopy(new_root))
            child_state.priority = state_id
            child_state.board = move(curr_board,'left', child_state)
            if child_state.board == goal:
                getResultsBFS(child_state, open_count)
                print("\nBreadth First Search is finished!\n")
                break;
            open.append(child_state)
            open_count += 1

        if col < 3: # MOVE RIGHT
            state_id += 1
            child_state = GameState(state_id, new_root.state_id, copy.deepcopy(new_root))
            child_state.priority = state_id
            child_state.board = move(curr_board,'right', child_state)
            if child_state.board == goal:
                getResultsBFS(child_state, open_count)
                print("\nBreadth First Search is finished!\n")
                break;
            open.append(child_state)
            open_count += 1

        if row < 3: # MOVE UP
            state_id += 1
            child_state = GameState(state_id,new_root.state_id, copy.deepcopy(new_root))
            child_state.priority = state_id
            child_state.board = move(curr_board,'down', child_state)
            if child_state.board == goal:
                getResultsBFS(child_state, open_count)
                print("\nBreadth First Search is finished!\n")
                break;
            open.append(child_state)
            open_count += 1


        if row > 0: # MOVE DOWN
            state_id += 1
            child_state = GameState(state_id,new_root.state_id,copy.deepcopy(new_root))
            child_state.priority = state_id
            child_state.board = move(curr_board,'up', child_state)
            if child_state.board == goal:
                getResultsBFS(child_state, open_count)
                print("\nBreadth First Search is finished!\n")
                break;
            open.append(child_state)
            open_count += 1


        closed.append(open[0])
        open.popleft()




if __name__ == '__main__':
    ### EXAMPLE STATES FROM HW ###
    # start=[[5,1,3,4],[2,10,6,8],[13,9,7,12],[0,14,11,15]] # Example 1
    # start=[[1,0,3,4],[5,2,7,8],[9,6,15,11],[13,10,14,12]] # Example 2
    # start=[[2,0,3,4],[1,5,7,8],[9,6,10,12],[13,14,11,15]] # Example 3
    # goal = [[1, 2, 3,4],[5, 6, 7,8], [9, 10, 11,12],[13, 14, 15,0]]


    ### INPUT STATES ###
    input_rows = 4
    print("\nPlease enter your START state with a space in between (ex. 1 0 3 4 5 2 7 8 9 6 15 11 13 10 14 12):")
    input_start = [int(n) for n in input().split()]
    start = [input_start[n:n+input_rows] for n in range(0, len(input_start), input_rows)]


    print("\nPlease enter your GOAL state with a space in between (ex. 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 0):")
    input_goal = [int(n) for n in input().split()]
    goal = [input_goal[n:n+input_rows] for n in range(0, len(input_goal), input_rows)]

    ### INITIALIZE START OBJECT AND PRIORITY QUEUES ###
    state = GameState(0, 0, None)
    state.set_board(start)
    open = deque([])
    closed = []
    root = state
    open.append(root)

    ### OPTIONS FOR SEARCHING ###
    bfs()

    # h_choice = 1
    # h_choice = 2
    # astar(h_choice)
