# Miranda's 15 Puzzle Solver
This 15-puzzle-solver is one solution for using breadth first search and A\* search algorithms to solve a 15-puzzle. This solution has two different heuristics that can be used for A* search.

[How to Run](#how-to-run)  
[Breadth First Search](#breadth-first-search)  
[A* Search](#breadth-first-search)  
[Inputs](#inputs)  
[Heuristics](#heuristicS)  


## How to Run

To run, execute the following command:
```
python3 MKDSourceCodeFile.py
```
Depending on which search algorithm you would like to use, different pieces of the source code will need to be commented out.

### Breadth First Search

To use the BFS search algorithm, the source code file should have the same commented lines are listed below.

```
### OPTIONS FOR SEARCHING ###
bfs()

# h_choice = 1
# h_choice = 2
# astar(h_choice)
```


These are the last lines of code in the MKDSourceCodeFile.py file.

### Inputs
Upon running MKDSourceCodeFile.py, the console with prompt for the user's input for the start state of the puzzle. The user should enter number 0-15 with a single space in between in the same order as the start state

```
Please enter your START state:
3 2 1 4 5 7 6 8 9 10 11 0 12 13 14 15
```
The user will then be prompted in the same way for a goal state.
```
Please enter your GOAL state with a space in between:
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
```
After entering the goal state, the solver will begin to run.

### A* using H1

To use the A* search algorithm with heuristic 1, the source code file should have the same commented lines are listed below.

```
### OPTIONS FOR SEARCHING ###
# bfs()

h_choice = 1
# h_choice = 2
astar(h_choice)
```

These are the last lines of code in the MKDSourceCodeFile.py file.


### A* using H2

To use the A* search algorithm with heuristic 2, the source code file should have the same commented lines are listed below.

```
### OPTIONS FOR SEARCHING ###
# bfs()

# h_choice = 1
h_choice = 2
astar(h_choice)
```

These are the last lines of code in the MKDSourceCodeFile.py file.

## Heuristics
A* search allows for the use of two different heuristic functions.

### H1

 The	estimated cost of taking a board B to the goal state G is the number of tiles in B that are not in the correct location as required by G.

### H2

The	estimated cost of taking a board B to the goal state G is the sum of the smallest number of moves for each tile, that is not at its	final location,	to reach its final location	as required by G.
